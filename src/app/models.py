from django.db import models
from django.contrib.auth.models import User


# Abstract model - с базой данных работают наследуемые классы, сам класс не работает и невидим для миграций

# Multi-table - работают с базой данных

# Proxy model - используются когда мы хотим изменить поведение модели. Например менеджер, доп. методы
# База данных не создаётся


class BaseBlog(models.Model):
    # поля, общие для всего приложения
    user = models.ForeignKey(User, related_name='%(app_label)s_related', null=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=120)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    # делаем модель абстрактной
    # невидима для миграций
    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class Bar(BaseBlog):
    bar_name = models.CharField(max_length=42)
    bar_age = models.IntegerField()


class Foo(models.Model):
    bar = models.CharField(max_length=42)

    def __str__(self):
        return self.bar


# Multi-table models

class BaseSite(models.Model):
    title = models.CharField(max_length=120)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class TextSIte(BaseSite):
    body = models.TextField()

    def __str__(self):
        return self.title

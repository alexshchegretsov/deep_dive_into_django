from django.contrib import admin
from .models import Bar, BaseSite, TextSIte

admin.site.register(Bar)
admin.site.register(BaseSite)
admin.site.register(TextSIte)
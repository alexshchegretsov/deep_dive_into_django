# -*- coding: utf-8 -*-

from django.urls import path, re_path
from django.conf.urls import url
from .views import serve_docs

urlpatterns = [
    # path('<path:path>/', serve_docs),
    url(r'^(?P<path>.*)$', serve_docs),
]

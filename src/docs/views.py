import os
from django.http import HttpResponse
from django.conf import settings
from django.contrib.staticfiles.views import serve


def serve_docs(request, path):

    docs_path = os.path.join(settings.DOCS_DIR, path)
    if os.path.isdir(docs_path):
        path = os.path.join(path, 'index.html')

    path = os.path.join(settings.DOCS_STATIC_NAMESPACE, path)
    return serve(request, path, insecure=True)

# BASE_DIR = '/home/alexander/Desktop/dev/spb_tut_ru/src'
# DOCS_DIR = '/home/alexander/Desktop/dev/spb_tut_ru/src/docs/static/project_docs_build'
# DOCS_STATIC_NAMESPACE = 'project_docs_build'

from django.db import models


class Person(models.Model):
    SELET_SIZES = (
        ("M", "Medium"),
        ("S", "Small"),
        ("L", "large"),
    )

    class Meta:
        abstract = True
        default_related_name = 'person'
        ordering = ["-created"]

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)

    # автозаполнение полей
    """
        def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)
        super(Course, self).save(*args, **kwargs)
    """

    # get_absolute_url()
